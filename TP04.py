import os

def continuar():
    print()
    input('presione una tecla para continuar ...')
    os.system('cls')

def menu():
    print('1) Cargar productos')
    print('2) Mostrar el listado de productos.')
    print('3) Mostrar los productos cuyo stock se encuentre en el intervalo[desde,hasta]')
    print('4) sume X al stock de todos los productos cuyo valor actual de stock sea menor al valor Y.')
    print('5) Eliminar todos los productos cuyo stock sea igual a cero ')
    print('6) salir')
    eleccion = int(input('Elija una opción: '))
    while not((eleccion >= 1) and (eleccion <= 7)):
        eleccion = int(input('Elija una opción: '))
    os.system('cls')
    return eleccion


def leerPrecio():
    precio = float(input('Precio: '))
    while not((precio >= 0)):
        precio = float(input('Precio: '))
    return precio
def leerStock():
    stock = float(input('Stock: '))
    while not((stock >= 0)):
        stock = float(input('Stock: '))
    return stock
def leerStockInf():
    stock = float(input('Stock del rango inferio: '))
    while not((stock >= 0)):
        stock = float(input('Stock del rango inferior: '))
    return stock
def leerStockSup():
    stock = float(input('Stock del rango superior: '))
    while not((stock >= 0)):
        stock = float(input('Stock  del rango superior: '))
    return stock

def mostrar(diccionario):
    print('Listado de productos')
    for clave,valor in diccionario.items():
        print(clave,valor)

def leerProducto():
    print('Cargar Lista de productos')
    productos = {}
    codigo = -1
    while (codigo != 0):
        codigo = int(input('CODIGO (cero para finalizar): '))
        if codigo != 0: 
            #datoDNI = estudiantes.get(dni,-1)
            #if (datoDNI == -1):
            if codigo not in productos:    
                descripcion = input('descripcion: ')
                precio = leerPrecio()
                stock = leerStock()
                productos[codigo] = [descripcion,precio,stock]
                print('agregado correctamente')
            else:
                print('el producto ya existee')

    return productos

def rangoStock(productos):
    print('Lista de los productos cuyo estock se encuentre en el intervalo[inf,sup]: ')
    stockInfe = leerStockInf()
    stockSupe = leerStockSup()
    for codigo, datos in productos.items():
        if (datos[2] >= stockInfe) and (datos[2] <= stockSupe):
            print(codigo, datos)
def stockSuma(productos):
    print('ingrese el valor de Y para realizar la suma a los stock menores a Y')
    y = leerStock()
    print('ingrese el valor de X a sumar a los stock menores a :', y)
    x = leerStock()
    for codigo, datos in productos.items():
        if datos[2]<y :
            datos[2] = datos[2]+x
def eliminarProductos(productos):
    listaNueva=[]
    for clave,datos in productos.items():
        if datos[2]==0:
            listaNueva.append(clave)
    eliminar(productos,listaNueva)
    listaNueva.clear()
def eliminar(a,lista):
    for item in lista:
        del a[item]
def salir():
    print('Fin del programa...')

opcion = 0
os.system('cls')
while (opcion != 6):
    opcion = menu()
    if opcion == 1:
        productos = leerProducto()
    elif opcion == 2:
        mostrar(productos)
        continuar()
    elif opcion == 3:
        rangoStock(productos)
        continuar()
    elif opcion ==4:
        stockSuma(productos)
        continuar()
    elif opcion == 5:
        eliminarProductos(productos)
        print('los produtos con stock igual a 0 se borraron correctamente')
        continuar()
    elif opcion == 6:
        salir()
        continuar()
        
